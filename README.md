###Generate Token

curl -X POST \
  http://localhost:8080/oauth/token \
  -H 'authorization: Basic dGFsazJhbWFyZXN3YXJhbjpteS1zZWNyZXQ=' \
  -H 'content-type: multipart/form-data' \
  -F grant_type=password \
  -F username=user \
  -F password=password \
  -F scope=openid
  
###Access Resource
  
  curl -X GET \
  http://localhost:8081/api \
  -H 'authorization: bearer some_access_token' \
  -H 'content-type: multipart/form-data' \
  -F grant_type=password \
  -F username=testing \
  -F password=password