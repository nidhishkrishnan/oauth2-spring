package com.digital.projects.demoauthserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

@Configuration
public class OAuth2Config extends AuthorizationServerConfigurerAdapter {

	private String clientId = "talk2amareswaran";
	private String clientSecret = "my-secret";
	private String publicKey = "-----BEGIN PUBLIC KEY-----\r\n"+
				"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyjMsAVNpTLl69Kn7qQ5L\r\n"+
				"CiwbvBiASASHHvvoIMct/eXWcO+lVEnUjIDKSDtjr6Z1IBz/ZdN32ptvM93hxL48\r\n"+
				"QFiK+cr1QFLS/UjnqjrniCQYZJmbyXXbWi/8/AnrYERn6NdINx34nnpIEWvhvLX9\r\n"+
				"El+houbhNssBF61ktRzd0y4tL3msnW1EKK1cQ9SWWQzqvzcRntSRRiBaCWmixsHL\r\n"+
				"EleGNLg5KfmEM0YkOhWH5tuwPAnTi+FGNebnh0O58PA8kFTMymYyPIgZg9jLLsys\r\n"+
				"vwk0OVavoeLWQ47ZVIf/IIx3w9pj+CKbk7sGkBblY6iQpypAv8p83zUw0jPguC8y\r\n"+
				"TwIDAQAB\r\n"+
				"-----END PUBLIC KEY-----";
	
	private String privateKey = "-----BEGIN RSA PRIVATE KEY-----\r\n"+
				"MIIEpAIBAAKCAQEAyjMsAVNpTLl69Kn7qQ5LCiwbvBiASASHHvvoIMct/eXWcO+l\r\n"+
				"VEnUjIDKSDtjr6Z1IBz/ZdN32ptvM93hxL48QFiK+cr1QFLS/UjnqjrniCQYZJmb\r\n"+
				"yXXbWi/8/AnrYERn6NdINx34nnpIEWvhvLX9El+houbhNssBF61ktRzd0y4tL3ms\r\n"+
				"nW1EKK1cQ9SWWQzqvzcRntSRRiBaCWmixsHLEleGNLg5KfmEM0YkOhWH5tuwPAnT\r\n"+
				"i+FGNebnh0O58PA8kFTMymYyPIgZg9jLLsysvwk0OVavoeLWQ47ZVIf/IIx3w9pj\r\n"+
				"+CKbk7sGkBblY6iQpypAv8p83zUw0jPguC8yTwIDAQABAoIBACYwiA0wDeFZ3uk6\r\n"+
				"+bcyZeXj8tER55iykjq95Vfbhso/kML/4EANOcHXyVzfKrLQQ6rvDyXSTP7TAKvR\r\n"+
				"KoUYURTuJEGNnciqXANOs8KpIXXLK3hEsvHX+rgL/EI2NWiXQvDtSExwsrZr7hSs\r\n"+
				"RZR44vWHbmXwKXVJ3kj0jOLQMu1ho0rMqG0EUokL5y+8FS8aSuA3G0pChqGG/xIN\r\n"+
				"idfCFsDkTGXoAR+BGEA0P2jpn3eALfWUGuhIS2o9mUv4aBvbYPhip++dB/80h5bW\r\n"+
				"SthPRKstRIvGA6pfebmd1YCaJ5Ww9TX3dNS12d7qf4RgPOsWbzst9fy8hzxD/3uE\r\n"+
				"QDHd/6ECgYEA7iOxa6z1XPZRNrCaLh2E4Gqz8FKhZtc8P90ziI8VVD3X6+uJUWR3\r\n"+
				"p448edCRCkxJkbLV1IYefTr2SiYtGb4teH3fB5tAhHW1izyfGDZM5n+ObbHb8z/T\r\n"+
				"75/xpok01Oadg7upTd5t3T9n8dFT2Xt0o1Oe8eqzZyJ0G2vwY1mUCecCgYEA2V1v\r\n"+
				"a6Imwbv3yylaJ84C2YKsq2IAmdMU5iglJTwZ3FD7QOUIijeK4l4MHTa+L2c0UUtS\r\n"+
				"WNtBZ9jgaOXKD1ZNhifPusvasci23fU0Hx/Vax1Qge04+qs0/rCxsRGRShRdKzvC\r\n"+
				"LHmIGZptqz/niz0Nnh9xTrGhfQwlpNvdvbAMF1kCgYEAnd8txLth4mItTvtfC0lo\r\n"+
				"iLpUVfMBq0LvX0tO2a45rqJdHbsFSt827+68qukY7mHKt/t+BKlxVwYatud+KL3K\r\n"+
				"OUIA9HL5H5dFZmwm8I83Bev0SUaLEUT0RLydIBF/49CLBgUH5WabIPzi9Q4X72sH\r\n"+
				"1SsfiTNWAf52SKOTNtnw6WsCgYBq7ElY2uTRvBMCfw74MaC5OkyR6Z1+DZVkOi87\r\n"+
				"h7r442UU4RU4WGYbQEQZQsp/KvdTXgfd6czctpR9RTwGG1/7NC49JvYnKiK6QFop\r\n"+
				"TVGAZWUq5HR46ishde2Sup+Ln0TNdZHoqsfGQG7eJItTtO6z76efHEXh603BMN58\r\n"+
				"5tKr0QKBgQCKBGoLpZO9EwK2L887MaWNFIHJZWRsQpOs8++Y9MI345khJSW1Lir6\r\n"+
				"gK0ibkW9xW0Ut7K/nbeeryJDlouwTe+U+mfXAI/PAQfbbWYG9KXbH+NIIt8g2i6z\r\n"+
				"i4tmSFuYP5Sfcbx7QXcJ6jJhAviFT2qQtXtL8TXVmj6h34o4Auf7EQ==\r\n"+
				"-----END RSA PRIVATE KEY-----";

	@Autowired
	@Qualifier("authenticationManagerBean")
	private AuthenticationManager authenticationManager;

	@Bean
	public JwtAccessTokenConverter tokenEnhancer() {
		JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
		converter.setSigningKey(privateKey);
		converter.setVerifierKey(publicKey);
		return converter;
	}

	@Bean
	public JwtTokenStore tokenStore() {
		return new JwtTokenStore(tokenEnhancer());
	}
	
	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		endpoints.authenticationManager(authenticationManager).tokenStore(tokenStore())
				.accessTokenConverter(tokenEnhancer());
	}
	
	@Override
	public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
		security.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()");
	}

	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {

		clients.inMemory().withClient(clientId).secret(clientSecret).scopes("read", "write")
				.authorizedGrantTypes("password", "refresh_token").accessTokenValiditySeconds(200)
				.refreshTokenValiditySeconds(20000).scopes("openid");

	}
	
}
